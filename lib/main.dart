import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

//Mis componentes
// import 'package:componentes/src/pages/homePage_temp.dart';
import 'package:componentes/src/pages/alert_page.dart';
import 'package:componentes/src/routes/routes.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Componentes App',
      debugShowCheckedModeBanner: false,
      localizationsDelegates: [
                                // ... delegado[s] de localización específicos de la app aquí
                                GlobalMaterialLocalizations.delegate,
                                GlobalWidgetsLocalizations.delegate,
                              ],
      supportedLocales: [
                          const Locale('en'), // Inglés
                          const Locale('es'), // Español
                        ],  
      //home:  HomePage()
      initialRoute: '/',
      routes: getRoutes(),
      onGenerateRoute: (RouteSettings settings){
          print('No se encontro la ruta: $settings');

          return MaterialPageRoute(
            builder: (context) => AlertPage()
          );
      },
    );
  }
}