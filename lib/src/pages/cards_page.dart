import 'package:flutter/material.dart';

class CardsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
                        title: Text('Cards Page'), 
                        centerTitle: true,
                      ),
        body: ListView(
                        padding: EdgeInsets.all(10.0),
                        children: <Widget>[
                          _cardTipo1(),
                          SizedBox(height: 10.0,),
                          _cardTipo2(),
                          SizedBox(height: 10.0,),
                          _cardTipo1(),
                          SizedBox(height: 10.0,),
                          _cardTipo2(),
                          SizedBox(height: 10.0,),
                          _cardTipo1(),
                          SizedBox(height: 10.0,),
                          _cardTipo2(),
                          SizedBox(height: 10.0,),
                        ],
                      ) 

    );
  }

  Widget _cardTipo1() {

    return Card(
                  elevation: 10.0,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
                  child: Column(
                    children: <Widget>[
                      ListTile(
                                leading: Icon(Icons.photo_album),
                                title: Text('Card Tipo 1'),
                                subtitle: Text('Aqui se pone la informacion que se desea mostrar en la card tipo 1, creado por Uriel Sánchez García'),
                              ),
                      Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                                                FlatButton(
                                                            child: Text('Cancelar'),
                                                            onPressed: (){},
                                                          ),
                                                FlatButton(
                                                            child: Text('Aceptar'),
                                                            onPressed: (){},
                                                          )
                            ],
                          )
                    ],
                  ),
              );

  }

  Widget _cardTipo2() {

    final card =  Container(
      //clipBehavior: Clip.antiAlias,
      child: Column(
                    children: <Widget>[
                                        FadeInImage(
                                                    image: NetworkImage('https://image.shutterstock.com/image-photo/colorful-hot-air-balloons-flying-260nw-1033306540.jpg'),
                                                    placeholder: AssetImage('assets/original.gif'),
                                                    height: 300.0,
                                                    fit: BoxFit.cover,
                                                    fadeInDuration: Duration(milliseconds: 200),
                                                  ),
                                        Container(
                                                    padding: EdgeInsets.all(10.0),
                                                    child: Text('Card Tipo 2')
                                                  ),

                                      ],
                  ),
    );

    return Container(
      decoration: BoxDecoration(

                                  borderRadius: BorderRadius.circular(30.0),
                                  color: Colors.white,
                                  boxShadow: <BoxShadow>[

                                                          BoxShadow(
                                                            color: Colors.black26,
                                                            blurRadius: 10.0,
                                                            spreadRadius: 1.0,
                                                            offset: Offset(0,10)
                                                          )

                                                        ]

                                ),
      child: ClipRRect(
                          borderRadius: BorderRadius.circular(30.0),
                          child: card, 
                      )

    );

  }
}