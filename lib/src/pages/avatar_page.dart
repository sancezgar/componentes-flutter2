import 'package:flutter/material.dart';

class AvatarPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
                      title: Text('Avatar Page'),
                      actions: <Widget>[
                        Container(
                                    margin: EdgeInsets.only(right: 10.0),
                                    child: CircleAvatar(
                                                          child: Text('U'),
                                                          backgroundColor: Colors.brown,
                                                        ),
                                  ),
                        Container(
                                    margin: EdgeInsets.symmetric(horizontal: 10.0,vertical: 5.0),
                                    child: CircleAvatar(
                                                          backgroundImage: NetworkImage('https://prod-discovery.edx-cdn.org/media/course/image/93f11b63-0c29-4472-964e-c6db1cc574e8-61863a8d0d90.small.jpg'),
                                                          radius: 25.0,
                                                        ),
                                  )
                      ],
                    ),
      body: Center(
                    child: FadeInImage(
                                        placeholder: AssetImage('assets/original.gif'),
                                        image: NetworkImage('https://prod-discovery.edx-cdn.org/media/course/image/93f11b63-0c29-4472-964e-c6db1cc574e8-61863a8d0d90.small.jpg'),
                                        height: 300.0,                  
                                        fit: BoxFit.cover,
                                        fadeInDuration: Duration(milliseconds: 200),
                                      ),            
                  ),

      
    );
  }
}