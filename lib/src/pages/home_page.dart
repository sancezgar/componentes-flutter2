import 'package:flutter/material.dart';
import 'package:componentes/src/providers/menu_provider.dart';
import 'package:componentes/src/utils/icono_string_util.dart';
//import 'package:componentes/src/pages/alert_page.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('HomePage'),
          centerTitle: true,
      ),
      body: _crearLista(),
    );
  }

  Widget _crearLista() {

    return FutureBuilder(
                future:  menuProvider.cargaDatos(),
                initialData: [],
                builder: (context, AsyncSnapshot<List<dynamic>> snapshot){

                  return ListView(
                    children: _listaItems(snapshot.data, context),
                  );

                },
            );
    
    // return ListView(
    //   children: _listaItems(),
    // );

  }

  List<Widget> _listaItems( List<dynamic> items, BuildContext context ) {

    final List<Widget> lista = [];

    for(Map item in items){

      Widget widgetTemp = ListTile(
                                    title: Text(item['texto']),
                                    leading: getIcon(item['icon']),
                                    trailing: Icon(Icons.arrow_forward_ios, color:Colors.blueAccent),
                                    onTap: (){

                                      // final route = MaterialPageRoute(
                                      //   builder: ( context ) => AlertPage()
                                      // );

                                      // Navigator.push(context,route);

                                      Navigator.pushNamed(context,item['ruta']);

                                    },
                                  );

      lista..add(widgetTemp)
            ..add(Divider());

    }

    return lista;

  }
}