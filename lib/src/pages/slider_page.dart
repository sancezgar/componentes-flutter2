import 'package:flutter/material.dart';

class SliderPage extends StatefulWidget {
  @override
  _SliderPageState createState() => _SliderPageState();
}

class _SliderPageState extends State<SliderPage> {

  double _tamano        = 100.0;
  bool   _bloqueaSlider = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Slider Page'),
      ),
      body: AnimatedContainer(
        curve: Curves.fastOutSlowIn,
        duration: Duration(milliseconds: 200),
        margin: EdgeInsets.only(top: 50.0),
        child: Column(
          children: <Widget>[
            _crearSlider(),
            _crearCheckbox(),
            _crearSwitch(),
            Expanded(child: _crearImagen()),
          ],
        ),
      ),
    );
  }

  Widget _crearSlider() {

    return Slider(
      value: _tamano, 
      min: 10,
      max: 400,
      activeColor: Colors.indigoAccent,
      //divisions: 20,
      label: 'Tamaño de la imagen',
      onChanged: (_bloqueaSlider)? null : (valor){
        setState(() {
          _tamano = valor;
        });
      }
    );

  }

  Widget _crearCheckbox() {

    return CheckboxListTile(
      title: Text('Bloquear slider'),
      value: _bloqueaSlider, 
      onChanged: (valor){

        setState(() {
          _bloqueaSlider = valor;
        });

      }
    );

  }

  Widget _crearSwitch() {

    return SwitchListTile(
      title: Text('Bloquear slider'),
      value: _bloqueaSlider, 
      onChanged: (valor){

        setState(() {
          _bloqueaSlider = valor;
        });

      }
    );

  }

  Widget _crearImagen() {

    return FadeInImage(
      placeholder: AssetImage('assets/original.gif'),
      image: NetworkImage('https://logosmarcas.com/wp-content/uploads/2018/05/Batman-Emblema.png'),
      width: _tamano,
      fit:BoxFit.contain

    );

  }

  
}