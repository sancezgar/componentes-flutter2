import 'package:flutter/material.dart';


class InputPage extends StatefulWidget {
  @override
  _InputPageState createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {

  String _nombre    = '';
  String _email     = '';
  String _password  = '';
  String _fecha     = '';
  List   _poderes   = ['Volar','Rayos X','Super Aliento','Super Fuerza'];
  String _opcionSeleccionada = 'Volar';

  TextEditingController _inputDateController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Inputs Page'),
      ),
      body: ListView(
                      padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                      children:<Widget>[
                                          _crearInput(),
                                          Divider(height: 10.0,),
                                          _inputEmail(),
                                          Divider(height: 10.0,),
                                          _inputPassword(),
                                          Divider(height: 10.0,),
                                          _inputDate(context),
                                          Divider(height: 10.0,),
                                          _inputDropDown(),
                                          Divider(height: 100.0,),
                                          _mostrarPersona(),
                                        ] ,
                    )
    );
  }

  Widget _crearInput() {

    return TextField(
      autofocus: false,
      decoration: InputDecoration(
                                    border: OutlineInputBorder(
                                                                borderRadius: BorderRadius.circular(20.0)
                                                              ),
                                    counter: Text('Letras: ${_nombre.length}'),
                                    hintText: 'Coloca tu nombre',
                                    prefixIcon: Icon(Icons.supervised_user_circle),
                                    suffixIcon: Icon(Icons.accessibility),
                                    labelText: 'Nombre',
                                    helperText: 'Solo coloca tu nombre'
                                  ),
      onChanged: (valor){
                          setState(() {
                            _nombre = valor;
                          });
                        },
      textCapitalization: TextCapitalization.words,
    );

  }

  Widget _inputEmail() {

    return TextField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
                                    border: OutlineInputBorder(
                                                                borderRadius: BorderRadius.circular(20.0)
                                                              ),
                                    hintText: 'Coloca tu email',
                                    prefixIcon: Icon(Icons.email),
                                    suffixIcon: Icon(Icons.alternate_email),
                                    labelText: 'Email',
                                  ),
      onChanged: (valor){
                          setState(() {
                            _email = valor;
                          });
                        },
    );

  }

  Widget _inputPassword() {

    return TextField(
      obscureText: true,
      decoration: InputDecoration(
                                    border: OutlineInputBorder(
                                                                borderRadius: BorderRadius.circular(20.0)
                                                              ),
                                    hintText: 'Coloca tu password',
                                    prefixIcon: Icon(Icons.lock),
                                    suffixIcon: Icon(Icons.lock_open),
                                    labelText: 'Password',
                                  ),
      onChanged: (valor){
                          setState(() {
                            _password = valor;
                          });
                        },
    );

  }

  Widget _inputDate( BuildContext context ) {


    return TextField(
      controller: _inputDateController,
      decoration: InputDecoration(
                                    border: OutlineInputBorder(
                                                                borderRadius: BorderRadius.circular(20.0)
                                                              ),
                                    hintText: 'Fecha',
                                    prefixIcon: Icon(Icons.calendar_today),
                                    suffixIcon: Icon(Icons.perm_contact_calendar),
                                    labelText: 'Fecha',
                                  ),
      onTap: (){
                  FocusScope.of(context).requestFocus(new FocusNode());
                  _selectDate( context );
                },
      
    );


  }

  void _selectDate(BuildContext context) async{


    DateTime picked = await showDatePicker(
                                          context: context, 
                                          initialDate: new DateTime.now(),
                                          firstDate: new DateTime(2018), 
                                          lastDate: new DateTime(2025),
                                          locale: Locale('es','MX')
                                        );
    if(picked != null){
      setState(() {
        _fecha = picked.toString();
        _inputDateController.text = _fecha;
      });
    }


  }

  List<DropdownMenuItem<String>> getOpcionesDropdown(){

    List<DropdownMenuItem<String>> lista = List();

    _poderes.forEach((poder){

      lista.add(DropdownMenuItem(

        child:Text(poder),
        value: poder,

      ));

    });

    return lista;

  }

  Widget _inputDropDown() {

    return Row(

      children: <Widget>[
                          Icon(Icons.select_all),
                          SizedBox(width:30.0),
                          Expanded(
                                    child: DropdownButton(
                                                            value: _opcionSeleccionada,
                                                            items:getOpcionesDropdown(),
                                                            onChanged: (opt){
                                                              setState(() {
                                                                _opcionSeleccionada=opt;
                                                              });
                                                            },
                                                          ),
                          ),
                        ],
    );
    

  }


  Widget _mostrarPersona() {

    return ListTile(
      title: Text('Tu nombre es: $_nombre'),
      subtitle: Column(
        children: <Widget>[
          Text('Tu correo es: $_email'),
          Text('Tu contraseña es: $_password'),
          Text('Tu fecha es: $_fecha'),
        ],
      ),
      trailing: Text("$_opcionSeleccionada"),
    );

  }




}
