import 'package:flutter/material.dart';

class HomePageTemp extends StatelessWidget {

  final items = ['Uno','Dos','Tres','Cuatro','Cinco'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Componentes'),centerTitle: true,),
      body: ListView(
        //children: _crearItems()
        children: _crearItemsCorto()
      )
    );
  }

  // List<Widget> _crearItems(){

  //   List<Widget> lista = new List<Widget>();

  //   for(String opt in items){

  //     final tempWidget = ListTile(
  //       title: Text(opt),
  //     );

  //     lista..add( tempWidget )
  //          ..add( Divider() );

  //   }

  //   return lista;

  // }

  List<Widget> _crearItemsCorto(){

    return items.map(( String opt ){

      return Column(
        children: <Widget>[
          ListTile(

            title: Text(opt),
            subtitle: Text('Cualquier cosa'),
            leading: Icon(Icons.accessible),
            trailing: Icon(Icons.arrow_forward_ios),
            onTap: (){},

          ),
          Divider()
        ],
      );

    }).toList();

    
  }

}