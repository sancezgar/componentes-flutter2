import 'package:flutter/material.dart';
import 'dart:async';

class ListaPage extends StatefulWidget {
  @override
  _ListaPageState createState() => _ListaPageState();
}

class _ListaPageState extends State<ListaPage> {
  //este controler nos ayuda saber los comportamientos del scroll
  ScrollController _scrollController = new ScrollController();
  //variables
  List<int> _lista = new List();
  int _ultimaImagen = 0;
  bool _isLoading = false;

//Inicializamos comportamientos que se deben ejecutar al inciar la página
  @override
  void initState() {
    super.initState();
    _agregar10();

    _scrollController.addListener((){
      if(_scrollController.position.pixels == _scrollController.position.maxScrollExtent){

        fetchData();

      }

    });
  }

//Comportamiento al cerrar la página que se visualiza
  @override
  void dispose() {
    super.dispose();
    //se elimina el objeto creado para optimizar la aplicación
    _scrollController.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Lista Page'),),
      //Stack funciona para poner contenedores en forma de cascada uno sobre otro
      body: Stack(
        children:<Widget>[
                            _crearListview(),
                            _crearLoading(),
                          ], 
      ),
    );
  }
  //creamos lo que contendra nuesto list view
  Widget _crearListview() {

    return RefreshIndicator(
          onRefresh: _refrescar,
          child: ListView.builder(
                                    controller: _scrollController,
                                    itemCount: _lista.length,
                                    itemBuilder: (BuildContext context, int index){
                                      
                                      int imagen = _lista[index];
                                      //cargamos los elementos dependiendo la cantidad que tenga el arreglo
                                      return FadeInImage(

                                        placeholder: AssetImage('assets/original.gif'),
                                        image: NetworkImage('https://i.picsum.photos/id/$imagen/500/300.jpg'),
                                        height: 300.0,
                                        fit: BoxFit.cover,

                                      );

                                    },
                                  ),
    );
  }
  //Este metodo es para cargar mas imagenes
  void _agregar10(){

    for (var i = 0; i < 10; i++) {
      
      _lista.add( _ultimaImagen++ );
      setState(() {
        
      });

    }

  }

  //se hace la simulacion de una peticion http
  Future<Null> fetchData() async{
    _isLoading = true;
    setState(() { });

    Duration duracion = new Duration(seconds: 2);
    //despues de esperar el tiempo estimado se ejecuta la funcion (2nd parametro)
    new Timer(duracion,httpcliente);


  }

  //la simulacion de que ya cargo el http
  void httpcliente(){
    _isLoading = false;

    _scrollController.animateTo(_scrollController.position.pixels+100, duration: Duration(milliseconds: 500), curve: Curves.slowMiddle);
    _agregar10();
  }

  Widget _crearLoading() {
    //se mostrara el widget cuando este cargando
    if(_isLoading){
      return Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                                          Row(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: <Widget>[
                                                                    CircularProgressIndicator(
                                                                      backgroundColor: Colors.white,
                                                                    ),
                                                                  ],
                                              ),
                                          SizedBox(height: 20.0,)
                                        ],

      );
    }else{
      //se mostrará un container vacio trasnparente
      return Container();
    }

  }

  Future<Null> _refrescar() async {

    Duration duracion = new Duration(seconds: 2);

    new Timer(duracion, (){
      _lista.clear();
      _ultimaImagen++;
      _agregar10();
    });

    return Future.delayed(duracion);

  }

}